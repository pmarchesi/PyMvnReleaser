
from .configuration import Configuration
from .proyect import Proycet
from .artifact import Artifact
import logging
from sys import exit


class Main():

    def __init__(self,configFile):
        self.config = Configuration(configFile)
        self.Artifact = Artifact(self.config)
        self.libs = {}
        self.root_proyect = ''
        self.__instanceProyects()


    def __instanceProyects(self):
        self.root_proyect = Proycet(self.config.getConfigs('root_projects')[0]['repo'], self.Artifact)
        for p in self.config.getConfigs('libs'):
            self.libs[p['name']] = Proycet(p['repo'],self.Artifact)

    def buildRoot(self):
        self.buildLibs(self.root_proyect)

    def prepareBuildLib(self,dependencies):
        for dependency in dependencies:
            found = False
            for lib in self.libs:
                if self.libs[lib].getType() == 'pom':
                    if dependencies[dependency]['groupId'] == self.libs[lib].getGroupId():
                        self.buildLibs(self.libs[lib])
                        found = True
                elif dependency == self.libs[lib].getArtifactId():
                        self.buildLibs(self.libs[lib])
                        found = True

            if not found:
                logging.error(msg="Dependencia no encontrada: " + dependency + " version " + str(dependencies[dependency]['version']) )
                exit()

    def buildLibs(self, objectLib):
        dependenciesForBuild = objectLib.testBuild()
        if dependenciesForBuild != 0:
            self.prepareBuildLib(dependenciesForBuild)
            objectLib.build()
        else:
            objectLib.build()
