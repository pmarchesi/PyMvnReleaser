from requests import get
import logging
from sys import exit


class Artifact():
    # TODO: no reconoce parent pom.
    def __init__(self, config):
        self.config = config
        self.baseUrl = self.config.getConfig('artifactory', 'url')
        self.verify = self.config.getConfig('artifactory', 'verify')
        self.user = self.config.getConfig('artifactory', 'username')
        self.password = self.config.getConfig('artifactory', 'password')

    def requestGet(self, url):
        try:
            if self.user and self.password:
                return get(self.baseUrl + url, verify=self.verify, auth=(self.user, self.password))

            else:
                return get(self.baseUrl + url, verify=self.verify)
        except:
            logging.error(msg="conection fail")
            exit()

    def urlGenerator(self, artifactGroup, artifactID, artifactVersion):
        return '/' + artifactGroup.replace('.', '/') + '/' + artifactID + '/' + artifactVersion

    def getStatusCode(self, artifactGroup, artifactID, artifactVersion):
        url = self.urlGenerator(artifactGroup, artifactID, artifactVersion)
        return self.requestGet(url)