from xml.dom import minidom
import re
import logging
class Pom(object):
    '''PASS'''

    def __init__(self,path):
        self.pomPath = str(path) + '/pom.xml'
        self.pom = minidom.parse(self.pomPath)
        self.goupId = self.__setGroupId
        self.artifactId = self.__setArtifactId()
        self.type = self.__setType()
        self.artifactVersion = self.__setArtifactVersion()
        self.dependencies = self.__setDependencies()

    def __setArtifactId(self):
        artifactId = self.pom.getElementsByTagName("artifactId")[0]
        return artifactId.firstChild.data

    def getArtifactId(self):
        return self.artifactId

    def __setArtifactVersion(self):
        artifactVersion = self.pom.getElementsByTagName("version")[0]
        return artifactVersion.firstChild.data

    def getArtifactVersion(self):
        return self.artifactVersion

    def __setGroupId(self):
        artifactId = self.pom.getElementsByTagName("groupId")[0]
        return artifactId.firstChild.data

    def getGroupId(self):
        return self.goupId

    def __setDependencies(self) -> object:
        deps = self.pom.getElementsByTagName("dependency")
        dependencies = dict()

        pattern = re.compile("\$\{")
        patternSnap = re.compile(".*-SNAPSHOT")

        for dep in deps:
            dic = dict()
            dic["groupId"] = dep.getElementsByTagName("groupId")[0].firstChild.data
            artifactId = dep.getElementsByTagName("artifactId")[0].firstChild.data
            try:
                dic["type"] = dep.getElementsByTagName("type")[0].firstChild.data
            except:
                dic["type"] = 'jar'

            version = dep.getElementsByTagName("version")[0]
            if patternSnap.match(version.firstChild.data):
                version.firstChild.replaceWholeText(str(version.firstChild.data).replace('-SNAPSHOT',''))
                dic['version'] = str(version.firstChild.data).replace('-SNAPSHOT','')
            else:
                dic['version'] = version.firstChild.data

            if "${project.version}" == dic['version']:
                dic['version'] = self.artifactVersion

            if pattern.match(dic['version']):
                dic['version'] = self.__getPropertyVersion(re.findall(r"[\w,-]+", dic['version'])[0])

            dependencies[artifactId] = dic

        self.pom.writexml(open(self.pomPath,'w'))
        return(dependencies)

    def getDependencies(self):
        return self.dependencies

    def __getPropertyVersion(self, tag):
        patternSnap = re.compile(".*-SNAPSHOT")
        properties = self.pom.getElementsByTagName("properties")[0]
        property = properties.getElementsByTagName(tag)[0]
        if patternSnap.match(property.firstChild.data):
            property.firstChild.replaceWholeText(str(property.firstChild.data).replace('-SNAPSHOT', ''))
            return(str(property.firstChild.data).replace('-SNAPSHOT',''))
        else:
            return property.firstChild.data

    def __setType(self):
        try:
            packaging = self.pom.getElementsByTagName("packaging")[0]
            return packaging.firstChild.data
        except:
            return "jar"

    def getType(self):
        return self.type

    def __setDependency(self,dependency,version):
        pass

    def removeSnapshot(self):
        pattern = re.compile(".*-SNAPSHOT")
        for dependency in self.dependencies:
            if pattern.match(self.dependencies[dependency]['version']):
                self.__setDependency(dependency,self.dependencies[dependency]['version'])
