Ej de config:

```
root_projects:
  - { name: web1, repo: 'git@gitlab.com:nicolaslino1/alan_mvn_app1.git'}

libs:
  - { name: lib1, repo: 'git@gitlab.com:nicolaslino1/alan_lib_1.git' }
  - { name: lib2, repo: 'git@gitlab.com:nicolaslino1/alan_lib_2.git' }

artifactory:
  url: http://scm.primary/artifactory/repo
  # ssl cert verify
  verify: false
  # user and pass are not required
  username:
  password:

```